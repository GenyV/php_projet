<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22/11/2018
 * Time: 16:55
 */

namespace mywishlist\Vues;


use Slim\Slim;

class VueInscription extends AbstractView
{

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $html='
            <div class="text-center">

                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="heading text-center">
                        <h1>Inscription</h1>
                        <div class="separator"></div>
                   </div>

                   <form id="contact-form" method="post" action="" role="form">

                

            <div class="controls col-md-offset-4">

         <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="username">Nom d\'utilisateur *</label>
                    <div class="separator2"></div>
                    <input id="username" type="text" name="username" class="form-control" placeholder="Entrez votre nom d\'utilisateur*" required="required" data-error="Entrez un nom de liste">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pass">Mot de passe *</label>
                    <div class="separator2"></div>
                    <input id="pass" type="password" name="pass" class="form-control" placeholder="Entrez votre mot de passe"  required="required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            </div>
        
             <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pass2">Confirmer votre mot de passe *</label>
                    <div class="separator2"></div>
                    <input id="pass2" type="password" name="pass2" class="form-control" placeholder="Confirmer votre mot de passe" required="required" data-error="Une date de validité est obligatoire">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            </div>
       
       
            <div class="row">
                 <div class="col-md-6">
                       <input type="submit" class="btn btn-success btn-send" value="S\'inscrire">
                 </div>
             </div>
             </div>
            </form>';
        
        return $html;
    }

    protected function breadcrumbs()
    {
        $app = Slim::getInstance();

        $html = '<ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li class="active">S\'inscrire</li></ol>';
        return $html;
    }
}