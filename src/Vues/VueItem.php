<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/11/2018
 * Time: 23:22
 */

namespace mywishlist\Vues;


use mywishlist\models\Cagnotte;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\Utils\Utilisateur;
use Slim\Slim;

require_once ('vendor/autoload.php');
class VueItem extends AbstractView
{

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $racine = BASE_URL;
        $app = Slim::getInstance();
        $item = Item::where('token','=', $this->data['token'])->first();
        $titre = $item->nom;
        $liste=$item->liste()->first();
        $proprietaire=0;
        if ((isset($_COOKIE['listes']) && array_key_exists($liste->no, unserialize($_COOKIE['listes']))) || (isset($_COOKIE['user_id']) && $liste->user_id==$_COOKIE['user_id']) || (Utilisateur::estConnecte() && Utilisateur::getUser()->id == $liste->user_id))
            $proprietaire=1;
        $html='
            <div class="heading text-center">
                        <h1>'.$titre.'</h1><div class="separator"></div><br>
                        
            </div>
             
            <div class="row text-center">';
                if ($item->img !="") {
					if (strpos($item->img, 'http')!==false){
						$lienImage = $item->img;
					} else {
                    $lienImage = BASE_URL.$item->img;
					}
                    $html .= '<img src="' . $lienImage . '" class="img-responsive center-block">';
                } if ($item->url !="")

                    $html.='<div class="row"><a href="'.$item->url.'" target="_blank"><button type ="button" class="btn btn-info">Plus d\'infos</button></a></div>';
                $bouton="Ajouter";
                if ($proprietaire==1) {
                    if ($item->img != "")
                        $bouton = "Modifier";
                    $html .= '
                        <div class="col-sm-12 col-lg-6 col-lg-offset-3">
                            <form class="form-inline" method="post">
                               <input type="text" style="width:350px"  name="lienImg" class="input-sm form-control" required="required" placeholder="Lien vers une image ou nom si elle existe déjà">
                               <button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-picture"></span>' . $bouton . '</button>
                            </form>
                        </div>';
                    $html .= '
                        <div class="col-sm-12 col-lg-6 col-lg-offset-3">
                            <form class="form-inline" method="post" enctype="multipart/form-data" action ="">
                               <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
                               <input type="file" style="width:350px"  name="fileImg" class="input-sm form-control" required="required" placeholder="Transferer une image">
                               <button type="submit" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-download-alt"></span>Transférer</button>
                            </form>
                        </div>';
                    if ($item->img != "")
                        $html .= '<div class=" row col-sm-12 col-lg-6 col-lg-offset-3">
                            <a href="' . $app->urlFor('liste_item_image_delete', ['token' => $this->data['token']]) . '" <button type="button" class="btn btn-danger">Supprimer l\'image</button></a>
                        </div>';
                    $cagnotte = $item->cagnotte()->first();
                    if (!$cagnotte)
                        $html .= '<div class=" row col-sm-12 col-lg-6 col-lg-offset-3">
                            <a href="' . $app->urlFor('liste_item_cagnotte_create', ['token' => $this->data['token']]) . '" <button type="button" class="btn btn-success">Créer une cagnotte</button></a>
                            </div>';
                }

                        $cagnotte = $item->cagnotte()->first();
                    if($cagnotte) {
                        $progression = number_format ( $cagnotte->montant / $item->tarif *100, 0);

                        $html .= '<div class=" row col-sm-12 col-lg-6 col-lg-offset-3">
                            <h3>Montant actuel de la cagnotte : ' . $cagnotte->montant . '€/' . $item->tarif . '€</h3>
                            </div>
                            <div class="row row col-sm-12 col-lg-6 col-lg-offset-3">
                            <div class="progress">
                               <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width:' . $progression . '%"></div>
                            </div>
                            </div>';
                        if ($proprietaire==0) {
                            if ($progression!=100)
                                $html.='<div class=" row col-sm-12 col-lg-6 col-lg-offset-3"><form class="form-inline" method="post">
                                     <input type="number" min=0 
                                     max='.($item->tarif - $cagnotte->montant).' 
                                     step=0.01 style="width:350px"  name="montPart" class="input-sm form-control" placeholder="Montant de la participation">
                                     <button type="submit" class="btn btn-success">Participer</button></a>\';
                                     </form></div>
                            ';
                            else
                                $html.='<div class=" row col-sm-12 col-lg-6 col-lg-offset-3">
                            <span class="label label-success">Objectif atteint !</span></div>';
                        }
                    } else {
                        if ($item->reserve==1)
                            $html .= '<h1><span class="label label-success">Réservé </span></h1>';
                        else if ($proprietaire==0){
                            $html.='<div class=" row col-sm-12 col-lg-6 col-lg-offset-3">
                           <h3>Prix : '.$item->tarif.'€</h3>
                           <a href="'.$app->urlFor('liste_item_reserve_view', ['token' => $item->token]).'"><button type="button" class="btn btn-info">Reserver</button></a>
                         </div>';
                        }
                    }




                
             
            $html.='</div>
            <br>
            <div class="row text-center">';




           
        $html.='</div><br>';

        if (($item->liste()->first()->expiration<date('Y-m-d') && $proprietaire==1) || $proprietaire==0 ){
            if ($item->reserve==1){
                $participation = $item->participation()->first();
                $html .= '<div class="separator2"></div><br><div class="row text-center">
                <h4>Participant : '.$participation->nom.'</h4>';
                $message="Aucun message laissé";
                if ($participation->message != "")
                    $message=$participation->message;
                $html.='<p>'.$message.'</p>


                </div>';
            }
        }

        return $html;
    }

    protected function breadcrumbs()
    {
        $app = Slim::getInstance();
        $liste = Liste::where('no','=',Item::where('token','=',$this->data['token'])->first()->liste_id)->first();
        $lien = $app->urlFor('liste_view', ['token' => $liste->token]);
        if ($liste->proprietaire()->first() == Utilisateur::getUser() || (isset($_COOKIE['user_id']) && $_COOKIE['user_id']==$liste->user_id))
            $lien = $app->urlFor('liste_user_detail', ['token' => $liste->tokenModification]);

        $html = '<ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li><a href="'.$lien.'">'.$liste->titre.'</a></li><li class="active">'.Item::where('token','=',$this->data['token'])->first()->nom.'</li></ol>';
        return $html;
    }
}