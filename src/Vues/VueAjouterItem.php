<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/11/2018
 * Time: 17:31
 */

namespace mywishlist\Vues;

use mywishlist\models\Item;
use mywishlist\models\Liste;
use Slim\Slim;

require_once ('vendor/autoload.php');
class VueAjouterItem extends AbstractView
{

    protected function render()
    {
        $item = new Item();
        $titre = "Ajouter un item";
        $bouton="Créer";
        if (isset($this->data['id'])) {
            $item = Item::where('id','=',$this->data['id'])->first();
            $bouton="Modifier";
            $titre="Modifier un item";
        }
        $html='
            <div class="text-center">

                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="heading text-center">
                        <h1>'.$titre.'</h1>
                        <div class="separator"></div>
                   </div>

                   <form id="contact-form" method="post" action="" role="form">

                

            <div class="controls col-md-offset-4">';

        $html.='<div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="nomItem">Nom de l\'item *</label>
                    <div class="separator2"></div>
                    <input id="nomItem" type="text" name="nomItem" value="'.$item->nom.'" class="form-control" placeholder="Entrez le nom de l\'item" required="required" data-error="Entrez un nom d\'item">
                    <div class="help-block with-errors"></div>
                </div>
             </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="descItem">Description</label>
                    <div class="separator2"></div>
                    <textarea id="descItem" name="descItem" class="form-control" placeholder="Entrez une description" rows="4"></textarea>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="prixItem">Prix *</label>
                    <div class="separator2"></div>
                    <input id="prixItem"  value="'.$item->tarif.'" type="number" name="prixItem" min="0.0" step="0.01" class="form-control" placeholder="Entrez un prix" required="required" data-error="Un prix est obligatoire">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        
         <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="urlItem">URL</label>
                    <div class="separator2"></div>
                    <input id="urlItem"  value="'.$item->url.'" type="url" name="urlItem" class="form-control" placeholder="Lien détaillant le produit" >
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
       
        <div class="row">
            <div class="col-md-6">
              
                    <div class="help-block with-errors"></div>
               
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <input type="submit" class="btn btn-success btn-send" value="'.$bouton.'">
            </div>
        </div>
        
  
    </div>

</form>

                

            </div>

        </div>';
        return $html;
    }

    protected function breadcrumbs()
    {
        $app = Slim::getInstance();
        $html = '<ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li><a href="'.$app->urlFor("user_profil_view").'">Profil</a></li><li><a href="'.$app->urlFor('liste_user_detail', ['token' => $this->data['token']]).'">'.Liste::where('tokenModification','=',$this->data['token'])->first()->titre.'</a></li><li class="active">Ajouter un élément</li></ol>';
        return $html;
    }
}