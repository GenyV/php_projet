<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/11/2018
 * Time: 21:37
 */

namespace mywishlist\Vues;


use mywishlist\models\Liste;
use mywishlist\models\Users;
use Slim\Slim;
require_once ('vendor/autoload.php');
class VueCreateurs extends AbstractView
{

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $app = Slim::getInstance();
        $html =
            '<div class="heading text-center">
                        <h1> Nos créateurs </h1>
                        <div class="separator"></div>
             </div>';


        $html.='<div class="col-lg-2">
        <div class="panel panel-info">
        <div class="panel-heading">
        <h3 class="panel-title">Les meilleurs créateurs :</h3>
        </div>
        <ul class="list-group">';
        $users = \mywishlist\models\Liste::selectRaw('user_id,count(*) as c')->where('publique','=',1)->where('expiration','>=', date('Y-m-d'))->groupBy('user_id')->orderBy('c', 'DESC')->take(5)->get();
            foreach ($users as $u) {
                $html .='<div class="row">';
                $user = Users::where('id','=',$u->user_id)->first();
                $html .= "<li class=\"list-group-item text-dark\">";
				if (! is_null($user)){
					
                $html .= '<div class="col-md-2" ><a href="'.$app->urlFor('user_profil_view',['username' => $user->username]).'"> '.$user->username.'</a></div>';
                $html .= '<span class="badge">' . $u->c . '</span>';
                $html.='</li></div>';
				}
            }

        $html.='</ul></div></div>';

            $html .='<div class="col-lg-4 col-lg-offset-2 col-sm-12">';
            $html .= '<div class="panel panel-info">';
            $html .= '<ul class="list-group">';
            foreach (Users::get() as $user ){
                $listes = $user->listes()->where('publique','=',1)->where('expiration','>=',date('Y-m-d'))->get();
                if ($listes->count()>0) {
                        $html .='<div class="row">';
                        $html .= "<li class=\"list-group-item text-dark\">";
						if ($user->profil()->first()->imgProfil!="")
                        $html.='<div class ="col-md-2"><img src="'.BASE_URL.$user->profil()->first()->imgProfil.'" class="img-circle"></div>';
                        $html .= '<div class="col-md-2" ><a href="'.$app->urlFor('user_profil_view',['username' => $user->username]).'"> '.$user->username.'</a></div>';

                        $html .= '<span class="badge">' . $listes->count() . '</span>';
                        $html .= '</li></a> </div>';


                }
            }
       return $html;
    }

    protected function breadcrumbs()
    {
        $app = Slim::getInstance();

        $html = '</div><ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li class="active">Créateurs</li></ol>';
        return $html;
    }
}