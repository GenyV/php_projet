<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20/11/2018
 * Time: 20:37
 */

namespace mywishlist\Vues;


use mywishlist\models\Liste;
use mywishlist\models\Users;
use mywishlist\Utils\Utilisateur;
use Slim\Slim;

require_once('vendor/autoload.php');
class VueListes extends AbstractView
{

    protected function render()
    {

        $titre='Listes Publiques';
        $message="";
        if (isset($this->data['username'])) {
            $titre = "Listes de ".$this->data['username'];
            $this->data['auteur_id'] = Users::where('username','=',$this->data['username'])->first()->id;
        }

        if (isset($this->data['token'])) {
            $liste = Liste::where('token','=', $this->data['token'])->first();
            $titre = $liste->titre.'<span class="badge">Jusqu\'au '.date("d/m/Y",strtotime($liste->expiration)).'</span>';
            $message = Liste::where('token','=', $this->data['token'])->first()->message;
        }
        $html='
            <div class="heading text-center">
                        <h1>'.$titre.'</h1>
                        <div class="separator"></div><br>
                        <h3>'.$message.'</h3>
             </div>
        <div class="col-lg-2">';
        $html.='<div class="panel panel-info">';
        $html.='<div class="panel-heading">';
        $html.='<h3 class="panel-title">Les listes déjà éxistantes:</h3>';
        $html.='</div>';
        $html.='<ul class="list-group">';


        $listes = \mywishlist\models\Liste::where('publique','=',1)->where('valide','=','1')->where('expiration','>=', date('Y-m-d'))->orderBy('expiration')->get();

        $app = Slim::getInstance();

        if (isset($this->data['auteur_id']) && $this->data['auteur_id']!=-1) {

            $listes = $listes->where('user_id','=',$this->data['auteur_id']);
        }

        if (isset($this->data['date1']) && $this->data['date1']!="") {

            $listes = $listes->where('expiration','>=',$this->data['date1']);
            $listes = $listes->where('expiration','<=',$this->data['date2']);
        }

        foreach ($listes as $liste) {
            if ($liste->expiration>=date("Y-m-d")) {
                $html.="<a href=\"".$app->urlFor('liste_view', ['token' => $liste->token])."\"> <li class=\"list-group-item text-dark\">";
                $html.='<span class="badge">'.$liste->items()->count().'</span>';
                $html.= $liste->titre.'</li></a>';
            }
        }



        $html.='<a href="'.$app->urlFor('listes_creer').'"><li class="list-group-item text-dark">+ Créer une liste</li></a>';

        $html.='</ul>';
        $html.='</div>';
        $html.='</div>';

        $html.='<div class="col-md-2 col-md-offset-6"></div> <form class="form-inline">
           
            <a href="'.$app->urlFor('listes_search_view').'"><button type ="button" class="btn-md btn-primary"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button></a>
          </div>';
        if (count($listes)==0 && !isset($this->data['token'])) {
            $html .='<div class="col-lg-8 col-lg-offset-1">
                        <h2>Aucune liste publique</h2>
                    </div>';

        }
        if (isset($this->data['token'])){
            $html.='<div class="container-fluid">
            <div class="col-lg-8 col-lg-offset-2">
            <ul class="list-group">';
            $liste = \mywishlist\models\Liste::where('token','=',$this->data['token'])->first();

            foreach ($liste->items()->get() as $item) {
                if ($item->token == ""){
                    $item->token = bin2hex(random_bytes(50));
                    $item->save();
                }
                $proprietaire=0;
                if ((isset($_COOKIE['user_id']) && $liste->user_id==$_COOKIE['user_id']) || (Utilisateur::estConnecte() && Utilisateur::getUser()->id == $liste->user_id))
                    $proprietaire=1;

                $html .= '<div class="row"> <div class="col-md-6"><a href="'.$app->urlFor('liste_item_view', ['token' => $item->token]).'"><li class="list-group-item text-dark">'. $item->nom;
                    if ($proprietaire==1 && $item->reserve==1)
                        $html.='<span class="label label-success pull-right">Reservé</span>';

                $html .= '</li></a></div></div>';
            }
            $html.='
             </ul>
             </div>
        </div>';
        } else if (count($listes)>0){
            $html .='<div class="col-lg-8 col-lg-offset-1">
                        <h2>Aucune liste sélectionnée</h2>
                    </div>';
        }



        return $html;
    }


    protected function breadcrumbs()
    {
        $app = Slim::getInstance();

    $html = '<ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li class="active">Listes</li></ol>';
    return $html;

    }
}