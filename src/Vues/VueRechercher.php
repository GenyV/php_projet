<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/11/2018
 * Time: 22:08
 */

namespace mywishlist\Vues;


use Slim\Slim;

require_once ('vendor/autoload.php');
class VueRechercher extends AbstractView
{

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $html='
            <div class="text-center">

                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="heading text-center">
                        <h1>Rechercher</h1>
                        <div class="separator"></div>
                   </div>

                   <form id="contact-form" method="post" action="" role="form">
                        <div class="controls col-md-offset-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nomAuteur">Nom de l\'auteur </label>
                                        <div class="separator2"></div>
                                        <input id="nomAuteur" type="text" name="nomAuteur" class="form-control" placeholder="Entrez le nom de l\'auteur">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="date1">Date de début de la recherche</label>
                                        <div class="separator2"></div>
                                        <input id="date1" type="date" name="date1" class="form-control" placeholder="Entrez une date de début">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="date2">Date de fin de la recherche</label>
                                        <div class="separator2"></div>
                                        <input id="dateListe" type="date" name="date2" class="form-control" placeholder="Entrez une date de fin" >
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="submit" class="btn btn-success btn-send" value="Rechercher">
                                </div>
                            </div>
                        </div>
                   </form>
                </div>
             </div>';

        return $html;
    }

    protected function breadcrumbs()
    {
        $app = Slim::getInstance();

        $html = '<ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li class="active">Rechercher</li></ol>';
        return $html;
    }
}