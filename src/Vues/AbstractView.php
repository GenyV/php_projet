<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20/11/2018
 * Time: 20:39
 */

namespace mywishlist\Vues;


use mywishlist\Utils\Utilisateur;

abstract class AbstractView
{
    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected abstract function render();
    protected $data;

    protected abstract function breadcrumbs();

    public function renderAll($data = [])
    {
        $this->data = $data;
        $vueBootstrap = new VuePageHTMLBootStrap();
        $content = $vueBootstrap->renderTop();
        $content .= $this->breadcrumbs();
        $content .= $vueBootstrap->renderAlerts();
        $content .= $this->render();
        $content .= $vueBootstrap->renderBottom();
        return $content;
    }

}