<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22/11/2018
 * Time: 23:33
 */

namespace mywishlist\Vues;


use mywishlist\Utils\Utilisateur;
use mywishlist\Models\Users;
use Slim\Slim;

class VueProfil extends AbstractView
{

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $app = Slim::getInstance();
        $proprietaire=0;
        if (Utilisateur::estConnecte() && $this->data['username'] == Utilisateur::getUser()->username)
            $proprietaire=1;
		$user = Users::where('username','=',$this->data['username'])->first();
       
            $html ='<div class="heading text-center">
                    <h1>Profil de '.$this->data['username'].'</h1>
                    <div class="separator"></div>
                    </div>
        <div class="container text-center">
            <div class=" col-md-4 pull-left">
            <div class="row">
                <img src="'.BASE_URL.$user->profil()->first()->imgProfil.'" class="img-circle">
            </div>
            <br>
            <div class="row">';


            if ($proprietaire==0)
            $html.='<a href="'.$app->urlFor('user_profil_listes_view',['username' => $user->username]).'"><button type="button" class="btn btn-info">Toutes ses listes</button></a>';
            else
                $html.='<a href="'.$app->urlFor('user_liste_view',['username' => $user->username]).'"><button type="button" class="btn btn-info">Toutes mes listes</button></a>';
            $html.='</div>
            <!--<div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <h3 class="text-primary">'.$user->username.'</h3>
                </div>
            </div>-->
            </div>
             <div class="col-md-8" style="vertical-align: middle">';
			 if ($proprietaire==1) {
			     $profil = Utilisateur::getUser()->profil()->first();
                 $html .= '<div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <form class="form-inline" method="post" enctype="multipart/form-data" action ="">
                               <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
                               <input type="file" style="width:350px"  name="fileImg" class="input-sm form-control" required="required" placeholder="Changer l\'image">
                               <button type="submit" class="btn-lg btn-info"><span class="glyphicon glyphicon-picture"></span>Changer l\'image</button>
                               <a href="' . $app->urlFor('user_profil_deleteImg', ['username' => Utilisateur::getUser()->username]) . '"><button type="button" class="btn-lg btn-danger">Supprimer l\'image</button></a>
                            </form>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <form class="form-group" method="post" action ="">';

                 $placeholder = "Entrez une description (max 255 caractères)";
                 $bouton = "Ajouter une description";
                 if ($profil->description!=""){
                     $placeholder = $profil->description;
                     $bouton = "Modifier la description";
                 }

                        $html.='<div class="row col-md-12">
                                <textarea id="descProfil" name="descProfil" class="form-control" placeholder="'.$placeholder.'" rows="4">' . Utilisateur::getUser()->profil()->first()->description . '</textarea>
                                </div>
                                <button type="submit" class="btn-lg btn-info">'.$bouton.'</button>';
                        if ($profil->description!="")
                               $html.='<a href="' . $app->urlFor('user_profil_deleteDesc', ['username' => Utilisateur::getUser()->username]) . '"><button type="button" class="btn-lg btn-danger">Supprimer la description</button></a>';
                            $html.='</form>
                        </div>
                     </div>
                     ';
             } else {
                 $profil = Users::where('username','=',$this->data['username'])->first()->profil()->first();
                 $html.=' <div class="row text-justify">
                <p>'.$profil->description.'</p> 
                </div>
               ';
             }

            $html.='</div>';
            
                   
            if ($proprietaire==1)    
            $html.='
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <form id="contact-form" method="post" action="" role="form" class="col-md-12 col-xs-12 col-sm-12">
                    <h3 class="text-primary ">Ajouter une liste existante : <input id="addliste" type="text"  name ="addliste" class="form-control" placeholder="Inserez ici l\'url de modification de la liste" required="required"></h3>
                            
                            <input type="submit" class="btn btn-success btn-send" value="Ajouter">
                    </form>
                </div>
            </div>
            
                    <br>
                    <div class="separator2"></div>
                    <br>
                   
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">
                   <a href="'.$app->urlFor('user_mdp_change',['username' => Utilisateur::getUser()->username]).'"><button type="button" class="btn btn-warning">Changer de mot de passe</button></a>
                </div>
            </div>
            
                    <br>
            
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">
                   <a href="'.$app->urlFor('user_delete',['username' => Utilisateur::getUser()->username]).'"><button type="button" class="btn btn-danger">Supprimer mon compte</button></a>
                </div>
            </div>
        </div>
        
        
        ';
        

        return $html;
    }

    protected function breadcrumbs()
    {
        $app = Slim::getInstance();
        $html = '<ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li class="active">Profil</li></ol>';
        return $html;
    }
}