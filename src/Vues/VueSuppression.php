<?php
/**
 * Created by PhpStorm.
 * User: geny17u
 * Date: 23/11/2018
 * Time: 13:34
 */

namespace mywishlist\Vues;

use mywishlist\Utils\Utilisateur;
use Slim\Slim;

require_once ('vendor/autoload.php');
class VueSuppression extends AbstractView
{

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $app = Slim::getInstance();
        $html='
        <div class="container">
            <div class="heading text-center">
                <h1>Supprimer mon compte</h1>
                   <div class="separator"></div>
            </div>  
            <div class="container text-center">
                 <div class="row">
                     <h3>Attention, vous êtes sur le point de supprimer votre compte et toutes les listes lui étant associées</h3>
                      <h3>Si vous êtes sûrs de vouloir supprimer votre compte, appuyez sur le bouton ci-dessous</h3>
                 </div>
                 <div class="row">
                 
                        <a href="'.$app->urlFor('user_delete_check',['username' => Utilisateur::getUser()->username]).'"><button type="button" class="btn btn-danger">Supprimer mon compte</button></a>
                 </div>
            
            </div>  
           
        </div>';

        return $html;
    }

    protected function breadcrumbs()
    {
        $app = Slim::getInstance();

        $html = '<ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li><a href="'.$app->urlFor("user_profil_view").'">Profil</a></li><li class="active">Supprimer mon compte</li></ol>';
        return $html;
    }
}