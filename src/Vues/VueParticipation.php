<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 24/11/2018
 * Time: 01:59
 */

namespace mywishlist\Vues;

use mywishlist\Utils\Utilisateur;
use Slim\Slim;

require_once ('vendor/autoload.php');
class VueParticipation extends AbstractView
{

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $app = Slim::getInstance();
        $eltParPage=15;
        $pageMax = Utilisateur::getUser()->participations()->count()/$eltParPage+1;
        if ($this->data['page']<=$pageMax)
            $pageAct = $this->data['page'];
        else
            $pageAct = $pageMax;
        if ($this->data['page']==0)
            $pageAct=1;
        $html = "";
        $html .= '
        <div class="heading text-center">
                        <h1>Mes participations</h1>
                        <div class="separator"></div>
        </div>
        <div class="row text-center">';

        if (Utilisateur::getUser()->participations()->count()==0) {
            $html .='<h3>Aucune participation :(</h3>"';
        } else {
            $participations = Utilisateur::getUser()->participations()->take($eltParPage)->skip($eltParPage*($pageAct-1))->get();
            $html.='<div class="row col-md-12 center-block">';
            $html.='<ul class="row col-md-4 col-md-offset-4">';
            foreach ($participations as $parti){
                $item = $parti->item()->first();
                $html.='<a href="'.$app->urlFor('liste_item_view', ['token' => $item->token]).'"><li class="list-group list-group-item">'.$item->nom.'</li></a>';
            }
            $html.='</ul>';
            $html.='</div>';
            /*$html .='<ul class="pagination"><li><a href="'.$app->urlFor('user_participation_view', ['page' =>$pageAct-1]).'">&laquo;</a></li>';
            if ($pageMax-$pageAct>=3) {
                $html.='

                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>

                ';
            }
            $html.='<li><a href='.$app->urlFor('user_participation_view', ['page' =>$pageAct+1]).'>&raquo;</a></li></ul>*/
            $html.='</div>';
        }


        return $html;
    }

    protected function breadcrumbs()
    {
        $app = Slim::getInstance();

        $html = '<ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li><a href="'.$app->urlFor("user_profil_view").'">Profil</a></li><li class="active">Mes Participations</li></ol>';
        return $html;
    }
}