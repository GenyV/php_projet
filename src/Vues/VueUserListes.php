<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22/11/2018
 * Time: 19:19
 */

namespace mywishlist\Vues;

use mywishlist\models\Liste;
use mywishlist\Utils\Alerte;
use mywishlist\Utils\Utilisateur;
use Slim\Slim;

require_once ('vendor/autoload.php');
class VueUserListes extends AbstractView
{

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $titre='Mes Listes';
        if (isset($this->data['token'])) {
            $liste = Liste::where('tokenModification','=', $this->data['token'])->first();
            if ($liste)  $titre = $liste->titre;

        }
            $app = Slim::getInstance();
            $html = "";
            $html .= '
        <div class="heading text-center">
                        <h1>'.$titre.'</h1>
                        <div class="separator"></div>
             </div>
         <div class="col-lg-2">';
            $html .= '<div class="panel panel-info">';
            $html .= '<div class="panel-heading">';
            $html .= '<h3 class="panel-title">Vos listes:</h3>';
            $html .= '</div>';
            $html .= '<ul class="list-group">';
            if (Utilisateur::estConnecte()) {
                $listes = Utilisateur::getUser()->listes()->get();

                foreach ($listes as $liste) {
                    $tokenModif = $liste->tokenModification;
                    if (is_null($tokenModif)) {
                        $tokenModif = bin2hex(random_bytes(25));
                        $liste->tokenModification = $tokenModif;
                        $liste->save();
                    }
                    $html .='<div class="row">';
                    $html .= "<a href=\"" . $app->urlFor('liste_user_detail', ['token' => $liste->tokenModification]) . "\"> <li class=\"list-group-item text-dark\">";
                    if ($liste->publique==0)
                        $html .= '<span class="label label-danger pull-left">Privée </span>';
                    else
                        $html .= '<span class="label label-success pull-left ">Publique </span>';
                    $html .= '<div class="col-md-6">'.$liste->titre.'</div>';
					
                    if ($liste->expiration<date("Y-m-d")) {
                        $html .= '<span class="label label-danger ">Expirée </span>';
                    } else if ($liste->valide == 0) {
                        $html .= '<span class="label label-warning ">Non validée </span>';
                    }
                    $html .= '<span class="badge">' . $liste->items()->count() . '</span>';
                    $html .= '</li></a> </div>';

                }
            }
            $html .= '<a href="' . $app->urlFor('listes_creer') . '"><li class="list-group-item text-dark">+ Créer une liste</li></a>';
            $html .= '</ul>';
            $html .= '</div>';
            $html .= '</div>';


            if (isset($this->data['token'])) {
                $html .= '
        <div class="container-fluid">
            <div class="col-md-8 col-md-offset-1">
                <ul class="list-group">';
                $liste = Liste::where('tokenModification', '=', $this->data['token'])->first();
                foreach ($liste->items()->get() as $item) {
                    if ($item->token == ""){
                        $item->token = bin2hex(random_bytes(50));
                        $item->save();
                    }
                    $html .= '<div class="row"> 
                                    <div class="col-md-6">
                                        <a href="'.$app->urlFor('liste_item_view', ['token' => $item->token]).'"><li class="list-group-item text-dark">'. $item->nom;
                                        if ($item->reserve==1) {
                                            $html .= '<span class="label label-success pull-right">Réservé </span>';
                                        } else {
                                            $cagnotte = $item->cagnotte()->first();
                                            if ($cagnotte) {
                                                $pourcentage = number_format ( $cagnotte->montant / $item->tarif *100, 0);
                                                $style = 'label-success';
                                                if ($pourcentage<75){
                                                    if ($pourcentage>=50)
                                                        $style = 'label-info';
                                                    else if ($pourcentage>25)
                                                        $style = 'label-warning';
                                                    else
                                                        $style = 'label-danger';
                                                }
                                                $html .= '<span class="label '.$style.' pull-right">Cagnotte à '.$pourcentage.'% </span>';
                                            }
                                        }

                                    $html.='</li></a></div>';

                        if ($item->reserve==0) {
                            $html .= '<div class="col-md-1">
                                        <a href="'.$app->urlFor('liste_item_modify_view',array('token' => $this->data['token'], 'id' => $item->id)).'"><button type="button" class="btn-md btn-info">Modifier</button></a>
                                      </div>
                                      <div class="col-md-1">
                                        <a href="'.$app->urlFor('liste_item_delete',array('token' => $this->data['token'], 'id' => $item->id)).'"><button type="button" class="btn-md btn-danger">Supprimer</button></a>
                                      </div>';
                        }
                    $html .= '</div>';
                }
                $html .= '<div class="row"> <div class="col-md-6"><a href="' . $app->urlFor('liste_item_addView',['token' => $this->data['token']]) . '"><li class="list-group-item text-dark">+ Ajouter un élément</li></a></div></div><br>
                <div class="row">';
                if ($liste->publique==0){
                    $html .= '<a href="' . $app->urlFor('listes_acces', array('token' => $liste->tokenModification, 'acces' => Liste::PUBLIC)) . '">Rendre la liste publique</a>';
                } else {
                   $html .= '<a href="' . $app->urlFor('listes_acces', array('token' => $liste->tokenModification, 'acces' => Liste::PRIVE)) . '">Rendre la liste privée</a>';
                }
                $html .= '</div><br>';
                if ($liste->valide==0)
                    $html .= '<div class="row"><div class="col-md-1"><a href="'.$app->urlFor('liste_validate',array('token' => $liste->tokenModification)).'"><button type="button" class="btn btn-success">Valider</button></a></div></div><br>';
                $html .= '<div class="row"><div class="col-md-1"><a href="'.$app->urlFor('liste_modify_view',array('token' => $liste->tokenModification)).'"><button type="button" class="btn btn-warning">Modifier</button></a></div></div><br>';
                $html .= '<div class="row"><div class="col-md-1"><a href="'.$app->urlFor('liste_share',['token' => $liste->tokenModification]).'"><button type="button" class="btn btn-info">Partager</button></a></div></div><br>';
                if (isset($this->data['sharelink']))
                    $html .='<p><a href="'.$this->data['sharelink'].'">'.$this->data['sharelink'].'</a></p>';
                $html .= '<div class="row"><div class="col-md-1"><a href="'.$app->urlFor('liste_suppr',['token' => $liste->token]).'"><button type="button" class="btn btn-danger">Supprimer</button></a></div></div>
                </ul>
             </div>
        </div>';

            } return $html;

    }

    protected function breadcrumbs()
    {
        $app = Slim::getInstance();

        $html = '<ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li><a href="'.$app->urlFor("user_profil_view").'">Profil</a></li><li class="active">Mes Listes</li></ol>';
        return $html;
    }
}