<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21/11/2018
 * Time: 18:51
 */

namespace mywishlist\Vues;


class Vue404 extends AbstractView
{

    protected function breadcrumbs()
    {
        return "";
    }

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        return <<<HTML
        <!-- Sections -->
        <section class="sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>Page non trouvée</h1>
                    <div class="separator2"></div>
                    <p>Aïe ! Aïe ! Aïe ! Il n'y a rien à voir par ici... La page recherchée est introuvable... ! Peut-être vous êtes vous trompés dans l'URL ?<br />Si le problème persiste, contactez les développeurs du site ! <em>(ils ne mordent pas !)</em></p>
                </div>
             </div>
         </section>

HTML;
    }


}