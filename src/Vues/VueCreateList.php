<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21/11/2018
 * Time: 21:47
 */

namespace mywishlist\Vues;


use mywishlist\models\Liste;
use Slim\Slim;
require_once ('vendor/autoload.php');
class VueCreateList extends AbstractView
{

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $liste = new Liste();
        $titre = "Créer une liste";
        $bouton = "Créer";

        if (isset($this->data['token'])) {
            $liste = Liste::where('tokenModification','=',$this->data['token'])->first();
            $bouton="Modifier";
            $titre="Modifier la liste";
        }


        $html='
            <div class="text-center">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="heading text-center">
                        <h1>'.$titre.'</h1>
                        <div class="separator"></div>
                   </div>
                   <form id="contact-form" method="post" action="" role="form">   
                        <div class="controls col-md-offset-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nomListe">Nom de la liste *</label>
                                        <div class="separator2"></div>
                                        <input id="nomListe" type="text" name="nomListe" value="'.$liste->titre.'" class="form-control" placeholder="Entrez le nom de la liste*" required="required" data-error="Entrez un nom de liste">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="descListe">Description</label>
                                        <div class="separator2"></div>
                                        <textarea id="descListe" name="descListe" class="form-control" placeholder="Entrez une description" rows="4">'.$liste->description.'</textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="messageListe">Message public</label>
                                        <div class="separator2"></div>
                                        <textarea id="messageListe" name="messageListe" class="form-control" placeholder="Entrez un message public" rows="4">'.$liste->message.'</textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dateListe">Date d\'expiration *</label>
                                        <div class="separator2"></div>
                                        <input id="dateListe" type="date" name="dateListe" value="'.$liste->expiration.'" class="form-control" placeholder="Entrez une date de validité" required="required" data-error="Une date de validité est obligatoire">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input id="publique" '.($liste->publique == Liste::PUBLIC ? 'checked="checked"':'').' type="checkbox" name="publique"> Rendre la liste publique<br/>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>                
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="submit" class="btn btn-success btn-send" value="'.$bouton.'">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>';

        return $html;
    }

    protected function breadcrumbs()
    {
        $app = Slim::getInstance();

        $html = '</div><ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li><a href="'.$app->urlFor("listes").'">Listes</a></li><li class="active">Créer</li></ol>';
        return $html;
    }
}