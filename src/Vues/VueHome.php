<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21/11/2018
 * Time: 18:58
 */

namespace mywishlist\Vues;

use Slim\Slim;

require_once('vendor/autoload.php');
class VueHome extends AbstractView
{

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {

        $app = Slim::getInstance();
        return <<<HTML
        <!-- Sections -->
        <section id="service" class="service sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>Nos services</h1>
                    <div class="separator"></div>
                </div>
                <!-- Example row of columns -->
                <div class="row">
                    <div class="wrapper">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="service-item text-center">
                                 <a href="{$app->urlFor("listes")}"> <i class="fa fa-list-ol"></i>
                                <h5>Listes</h5>
                                <div class="separator2"></div>
                                <p>Créez des listes de voeux pour vos événements.</p>
                            </div>
                        </div> </a>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="service-item text-center">
                                <i class="fa fa-check"></i>
                                <h5>Participation</h5>
                                <div class="separator2"></div>
                                <p>Participez à des listes et laissez votre message !</p>
                            </div>
                        </div> 

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="service-item text-center">
                                <i class="fa fa-send"></i>
                                <h5>Partage</h5>
                                <div class="separator2"></div>
                                <p>Partagez vos listes avec le monde entier !</p>
                            </div>
                        </div> 

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="service-item text-center">
                                <i class="fa fa-plus"></i>
                                <h5>Et bien plus !</h5>
                                <div class="separator2"></div>
                                <p>Ne laissez place qu'à votre créativité !</p>
                            </div>
                        </div> 

                    </div>
                </div>
            </div> <!-- /container -->       
        </section>
HTML;
    }

    protected function breadcrumbs()
    {
        return "";
    }
}