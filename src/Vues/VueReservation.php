<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 24/11/2018
 * Time: 00:23
 */

namespace mywishlist\Vues;


use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\Utils\Utilisateur;
use Slim\Slim;

require_once('vendor/autoload.php');
class VueReservation extends AbstractView
{

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $nom ="";
        if (Utilisateur::estConnecte()) $nom = Utilisateur::getUser()->username;
        if (!Utilisateur::estConnecte() && isset($_COOKIE['nomPart'])) $nom = $_COOKIE['nomPart'];
        $html = ' <form id="contact-form" method="post" action="" role="form">   
                        <div class="controls col-md-offset-4 text-center">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nomPart">Nom du participant *</label>
                                        <div class="separator2"></div>
                                        <input id="nomPart" type="text" name="nomPart" value="'.$nom.'" class="form-control" placeholder="Entrez le nom du participant*" required="required" data-error="Entrez un nom de liste">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                 </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="messagePart">Message</label>
                                        <div class="separator2"></div>
                                        <textarea id="messagePart" name="messagePart" class="form-control" placeholder="Entrez un message" rows="4"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="submit" class="btn btn-info btn-send" value="Réserver">
                                </div>
                            </div>
                         </div>
                 </form>';
        return $html;
    }

    protected function breadcrumbs()
    {
        $app = Slim::getInstance();
        $item = Item::where('token','=',$this->data['token'])->first();
        $liste = Liste::where('no','=',$item->liste_id)->first();
        $html = '<ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li><a href="'.$app->urlFor('liste_view', ['token' => $this->data['token']]).'">'.$liste->titre.'</a></li><li><a href="'.$app->urlFor('liste_item_view', ['token' => $item->token]).'">'.$item->nom.'</a></li><li class="active">Réserver</li></ol>';
        return $html;
    }
}