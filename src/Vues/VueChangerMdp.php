<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22/11/2018
 * Time: 23:54
 */

namespace mywishlist\Vues;


use mywishlist\Utils\Utilisateur;
use Slim\Slim;
require_once ('vendor/autoload.php');
class VueChangerMdp extends AbstractView
{


    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $html='
            <div class="text-center">

                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="heading text-center">
                        <h1>Changer de mot de passe</h1>
                        <div class="separator"></div>
                   </div>

                   <form id="contact-form" method="post" action="" role="form">

                

            <div class="controls col-md-offset-4">

         <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="username">Ancien mot de passe *</label>
                    <div class="separator2"></div>
                    <input id="oldpass" type="password" name="oldpass" class="form-control" placeholder="Entrez votre ancien mot de passe" required="required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pass">Nouveau mot de passe *</label>
                    <div class="separator2"></div>
                    <input id="pass" type="password" name="pass" class="form-control" placeholder="Entrez votre mot de passe"  required="required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            </div>
        
             <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pass2">Confirmer votre mot de passe *</label>
                    <div class="separator2"></div>
                    <input id="pass2" type="password" name="pass2" class="form-control" placeholder="Confirmer votre mot de passe" required="required" data-error="Une date de validité est obligatoire">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            </div>
       
       
            <div class="row">
                 <div class="col-md-6">
                       <input type="submit" class="btn btn-success btn-send" value="Valider">
                 </div>
             </div>
             </div>
            </form>';

        return $html;
    }

    protected function breadcrumbs()
    {
        $app = Slim::getInstance();

        $html = '<ol class="breadcrumb"><li><a href="'.$app->urlFor("root").'">Home</a></li><li><a href="'.$app->urlFor("user_profil_view",['username' => Utilisateur::getUser()->username]).'">Profil</a></li><li class="active">Changer de mot de passe</li></ol>';
        return $html;
    }
}