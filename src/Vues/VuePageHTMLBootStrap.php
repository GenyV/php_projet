<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20/11/2018
 * Time: 21:01
 */

namespace mywishlist\Vues;


use mywishlist\Utils\Alerte;
use mywishlist\Utils\Utilisateur;
use Slim\Slim;
require_once ('vendor/autoload.php');
class VuePageHTMLBootStrap
{

    public function renderTop(){
        $racine=BASE_URL;
        $app = Slim::getInstance();
        $menu = '<li><a href="'.$app->urlFor('root').'">Accueil</a></li>';
        $menu .= '<li><a href="'.$app->urlFor('createurs_view').'">Nos créateurs</a></li>';
        $menu .= '<li><a href="'.$app->urlFor('listes').'">Nos listes</a></li>';
        if(Utilisateur::estConnecte())
            $menu.= '<li><a href="'.$app->urlFor('user_profil_view', ['username' => Utilisateur::getUser()->username]).'">Profil</a></li>
                    <li><a href="'.$app->urlFor('listes_creer').'">Créer une liste</a></li>
                    <li><a href="'.$app->urlFor('user_liste_view').'">Mes listes</a></li>
                     <li><a href="'.$app->urlFor('user_participation_view', ['page' =>1]).'">Mes participations</a></li>
                    <li><a href="'.$app->urlFor('logout').'">Déconnexion</a></li>';
        else
            $menu .='<li><a href="'.$app->urlFor('listes_creer').'">Créer une liste</a></li>
                     <li><a href="'.$app->urlFor('login').'">Connexion</a></li>
                     <li><a href="'.$app->urlFor('register').'">Inscription</a></li>';
        $html='

        
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>My Wishlist App - By GENY Vanessa</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="'.$racine.'css/bootstrap.min.css">

        <link rel="icon" href="'.$racine.'favicon.ico" />

        <!--For Plugins external css-->
        <link rel="stylesheet" href="'.$racine.'css/plugins.css" />
        <link rel="stylesheet" href="'.$racine.'css/lora-web-font.css" />
        <link rel="stylesheet" href="'.$racine.'css/opensans-web-font.css" />
        <link rel="stylesheet" href="'.$racine.'css/magnific-popup.css">

        <!--Theme custom css -->
        <link rel="stylesheet" href="'.$racine.'css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="'.$racine.'css/responsive.css" />
        
     
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <nav class="navbar navbar-default navbar-fixed-top" id="main_navbar">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="'.$racine.'images/logo.png" alt="logo" /></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        '.$menu.'
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        
        </head>
        <!--Home page style-->
        <header id="home" class="home">
            <div class="overlay ">
                <div class="container-fluid">
              
                        <div class="home-wrapper">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="home-content text-center">
                                    <h1 class=>My WishList </h1>
                                    <h4>Créez et participez à des listes de voeux.</h4>
                                </div>
                            </div>
                        </div>
                
                </div>
            </div>
         
        </header>';

        return $html;
        
    }
    public function renderAlerts(){
        $html = "";
        foreach(Alerte::getAll() as $type => $tab)
        {
            foreach($tab as $message) {
                $html .= '<div class="fade in alert-dismissible alert alert-';
                switch($type) {
                    case 'success':
                        $html .= 'success';
                        break;
                    case 'info':
                        $html .= 'info';
                        break;
                    case 'error':
                        $html .= 'danger';
                        break;
                    case 'warning':
                        $html .= 'warning';
                        break;
                }
                $html .= '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$message.'</div>';
            }
        }
        Alerte::clear();
        return $html;


    }
    public function renderBottom(){
       return <<<HTML
       <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <div id="footer" class="footer">
                <div class="copyright ">
                    <p class="text-center">&copy; 2018 - Créé par GENY Vanessa dans le cadre du projet de WEB du S3</p>
                </div>
            </div>
        </div>
    </html>
HTML;
    }

}