<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22/11/2018
 * Time: 16:26
 */

namespace mywishlist\Utils;

class Utilisateur {

    /**
     * Fonction permettant de savoir si l'utilsateur est connecté
     * @return bool true si l'utilsateur est connecté
     */
    public static function estConnecte() {
        return isset($_SESSION['user']);
    }

    public static function saveUser($user)
    {
        $_SESSION['user'] = serialize($user);
    }

    public static function getUser(){
        if (Utilisateur::estConnecte()) {
            return unserialize($_SESSION['user']);
        }
        return null;
    }

    /**
     * Fonction permettant de deconnecter l'utilisateur
     */
    public static function deconnecter() {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }
    }

}