<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/11/2018
 * Time: 20:32
 */

namespace mywishlist\Controleurs;


use mywishlist\models\Cagnotte;
use mywishlist\models\Item;
use mywishlist\models\Liste;
use mywishlist\models\Participation;
use mywishlist\Utils\Alerte;
use mywishlist\Utils\Utilisateur;
use mywishlist\Vues\VueAjouterItem;
use mywishlist\Vues\VueItem;
use mywishlist\Vues\VueReservation;

require_once('vendor/autoload.php');
class ControleurItem
{

    /**
     * Fonction permettant d'afficher la vue pour ajouter un item
     * @param $token token de modification de la liste ou l'item doit etre ajoute
     */
    public function afficherAjoutItem($token) {
        echo (new VueAjouterItem())->renderAll(['token' => $token]);
    }


    /**
     * Fonction permettant d'ajouter un item dans la base de données
     * @param $token token de la liste
     */
    public function ajouterItem($token) {
        $liste = Liste::where('tokenModification','=',$token)->first();
        if (!$liste)
            Alerte::createThenRedirect('Liste inexistante', Alerte::WARNING, 'listes');
        $item = new Item();
        $item->nom = $_POST['nomItem'];
        $item->descr = $_POST['descItem'];
        $item->tarif = $_POST['prixItem'];
        $item->url = $_POST['urlItem'];
        $item->liste_id = $liste->no;
        $item->save();
        Alerte::createThenRedirect("Item ajouté avec succès", Alerte::SUCCESS,'liste_user_detail', ['token' => $token]);
    }

    /**
     * Fonction permettant d'afficher la vue d'un item
     * @param $token token de l'item
     */
    public function afficherItem($token) {
        $item = Item::where('token','=',$token)->first();
        if (!$item)
            Alerte::createThenRedirect("L'item n'existe pas", Alerte::WARNING,'listes');
		if (Utilisateur::estConnecte())
			$id = Utilisateur::getUser()->id;
		else if (isset($_COOKIE['user_id']))
			$id = $_COOKIE['user_id'];
		else
		    $id=-1;
		//Verification si l'utilisateur est proprietaire de la liste ou non
       if (($item->liste()->first()->publique==0 && !$id == $item->liste()->first()->user_id) && !(isset($_COOKIE['listes']) && array_key_exists($item->liste()->first()->no,unserialize($_COOKIE['listes']))))
            Alerte::createThenRedirect("L'item est privé", Alerte::WARNING,'listes');
        echo (new VueItem())->renderAll(['token' => $token]);


    }

    /**
     * Fonction faisant la gestion des 'posts' sur la vue d'un item
     * @param $token token de l'item
     */
    public function modifierImageOuParticiper($token) {
		
        $item = Item::where('token','=',$token)->first();
        if (!$item)
            Alerte::createThenRedirect("Cet item n'existe pas", Alerte::WARNING,'root');
        //Si on donne un lien pour l'image
        if (isset($_POST['lienImg'])){
            $racine=BASE_URL;
            $lienImage = $_POST['lienImg'];
                if (strpos($lienImage, 'web/img') !== false) {
                    if (!file_exists($lienImage))
                        Alerte::createThenRedirect("Cette image n'existe pas",Alerte::WARNING, 'liste_item_view', ['token' => $token]);
                    $lienImage = $racine . "/" . $lienImage;
                }
                if (strpos($lienImage, '/') == false) {
                    if (file_exists("web/img/" . $lienImage))
                        $lienImage = "web/img/" . $lienImage;
                    else if (file_exists("web/img/-1/" . $lienImage))
                        $lienImage = "web/img/-1/" . $lienImage;
                    else if (Utilisateur::estConnecte())
                        if (file_exists("web/img/".Utilisateur::getUser()->id.'/'. $lienImage))
                            $lienImage = "web/img/" .Utilisateur::getUser()->id.'/'. $lienImage;
                         else
                             Alerte::createThenRedirect("Cette image n'existe pas",Alerte::WARNING, 'liste_item_view', ['token' => $token]);
                }
            $item->img = $lienImage;
            $item->save();
            Alerte::createThenRedirect("Image modifiée avec succès", Alerte::SUCCESS,'liste_item_view', ['token' => $token]);
            //Si on transfere une image
        } else if (count($_FILES)==1) {
			var_dump($_SERVER);
            $extension = explode('.',$_FILES['fileImg']['name'])[count( explode($_FILES['fileImg']['name'],'.'))];
            if ($extension!="gif" && $extension!="png" && $extension!="jpg")
                Alerte::createThenRedirect("Extention non autorisée, extensions autorisées : .gif .png .jpg", Alerte::WARNING,'liste_item_view', ['token' => $token]);
            if ($_FILES['fileImg']['error'] > 0)
                Alerte::createThenRedirect("Erreur lors du transfert du fichier", Alerte::ERROR,'liste_item_view', ['token' => $token]);
            if ($_FILES['fileImg']['size'] > $_POST['MAX_FILE_SIZE'])
                Alerte::createThenRedirect("Erreur le fichier est trop volumineux", Alerte::ERROR,'liste_item_view', ['token' => $token]);
            $fileName=$_FILES['fileImg']['name'];
            if (Utilisateur::estConnecte() || isset($_COOKIE['user_id']) || (isset($_COOKIE['listes']) && array_key_exists($item->liste()->first()->no,unserialize($_COOKIE['listes'])))) {
				if (Utilisateur::estConnecte())
					$id = Utilisateur::getUser()->id;
				else if (isset($_COOKIE['user_id']))
					$id = $_COOKIE['user_id'];
				else {
                    $id = -1;
                    $fileName=date('Y-m-d-H-i-s').$fileName;
                }

                if (!file_exists('web/img/'.$id)) {
                    mkdir('web/img/'.$id, 0777, true);
                }
                if (!file_exists('web/img/'.$id.'/'.$fileName)) {
                    $transfert = move_uploaded_file($_FILES['fileImg']['tmp_name'],'web/img/'.$id.'/'.$fileName);
                    if ($transfert) {
                        $item->img = '/web/img/'.$id.'/'.$fileName;
                        $item->save();
                        Alerte::createThenRedirect("L'image a bien été transférée", Alerte::SUCCESS,'liste_item_view', ['token' => $token]);
                    }
                } else {
                    Alerte::createThenRedirect("Ce fichier existe déjà", Alerte::ERROR,'liste_item_view', ['token' => $token]);
                }
            } else {
				Alerte::createThenRedirect("Vous n'êtes pas le proprietaire de cette liste ! ", Alerte::WARNING,'liste_item_view', ['token' => $token]);
			}

            //Si on participe
        } else if (isset($_POST['montPart'])){
            $cagnotte = Cagnotte::where('id_item','=',$item->id)->first();
            if (!$cagnotte)
                Alerte::createThenRedirect("Cette cagnotte n'existe pas", Alerte::WARNING,'liste_item_view', ['token' => $token]);
            $cagnotte->montant = $cagnotte->montant+$_POST['montPart'];
            $cagnotte->save();
            Alerte::createThenRedirect("Merci pour votre participation à cette cagnotte :)", Alerte::SUCCESS,'liste_item_view', ['token' => $token]);
        }

    }

    /**
     * Fonction permettant de supprimer l'image de l'item
     * @param $token token de l'item
     */
    public function supprimerImage($token) {
        $racine = BASE_URL;
        $item = Item::where('token','=',$token)->first();
        if (!$item)
            Alerte::createThenRedirect("Cet item n'existe pas", Alerte::WARNING,'root');
        if(strpos($item->img,'/web/img/-1/'))
           unlink(substr(explode($_SERVER['HTTP_HOST'].'/',$item->img)[0],1,strlen(explode($_SERVER['HTTP_HOST'].'/',$item->img)[0])-1));
        $item->img = '';
        $item->save();
        Alerte::createThenRedirect("Image supprimée avec succès", Alerte::SUCCESS,'liste_item_view', ['token' => $token]);
    }


    /**
     * Fonction affichant la vue de modification de l'item
     * @param $token token de la liste
     * @param $id id de l'item
     */
    public function afficherModifierItem($token, $id) {
        $item = Item::where('id','=',$id)->first();
        if (!$item)
            Alerte::createThenRedirect("Cet item n'existe pas", Alerte::WARNING,'root');
        if ($item->reserve==1)
            Alerte::createThenRedirect("Vous ne pouvez pas modifier un item réservé", Alerte::WARNING,'liste_item_view', ['token' => $token]);
        echo (new VueAjouterItem())->renderAll(array('token' => $token,'id' => $id));
    }

    /**
     * Fonction permettant de creer une cagnotte
     * @param $token token de l'item
     */
    public function creerCagnotte($token) {
        $item = Item::where('token','=',$token)->first();
        if (!$item)
            Alerte::createThenRedirect("Cet item n'exizte pas", Alerte::WARNING,'root');
        if (Cagnotte::where('id_item','=',$item->id)->first())
            Alerte::createThenRedirect("Cet item a déjà une cagnotte", Alerte::WARNING, 'liste_item_view', ['token' => $token]);
        $cagnotte = new Cagnotte();
        $cagnotte->id_item = $item->id;
        $cagnotte->save();
        Alerte::createThenRedirect("Cagnotte créée avec succès", Alerte::SUCCESS, 'liste_item_view', ['token' => $token]);
    }

    /**
     * post de la vue modifier item
     * @param $token token de l'item
     * @param $id id de l'item
     */
    public function modifierItem($token, $id) {
        $item = Item::where('id','=',$id)->first();
        $item->nom = $_POST['nomItem'];
        $item->descr = $_POST['descItem'];
        $item->tarif = $_POST['prixItem'];
        $item->url = $_POST['urlItem'];
        $item->save();
        Alerte::createThenRedirect("Item modifié avec succès", Alerte::SUCCESS,'liste_user_detail', ['token' => $token]);
    }

    /**
     * Fonction permettant d'afficher la vue de reservation
     * @param $token token de l'item
     */
    public function afficherReserverItem($token) {
        if (Item::where('token','=',$token)->first()->reserve==1)
            Alerte::createThenRedirect("L'item est déjà reservé", Alerte::WARNING, 'liste_item_view', ['token' => $token]);
        echo (new VueReservation())->renderAll(array('token' => $token));
    }

    /**
     * Fonction permettant de reserver un item (Post)
     * @param $token token de l'item
     */
    public function reserverItem($token) {
        $item = Item::where('token','=',$token)->first();
        if ($item ->reserve==1)
            Alerte::createThenRedirect("L'item est déjà reservé", Alerte::WARNING, 'liste_item_view', ['token' => $token]);
        $participation = new Participation();
        if (Utilisateur::estConnecte())
            $participation->id_user = Utilisateur::getUser()->id;
        else {
            $participation->id_user = -1;
            setcookie("nomPart",$_POST['nomPart']);
        }

        $participation->nom = $_POST['nomPart'];
        $participation->message = $_POST['messagePart'];
        $participation->id_item =$item->id;
        $participation->save();
        $item->reserve = 1;
        $item->save();

        Alerte::createThenRedirect("Merci pour votre participation :)", Alerte::SUCCESS,'liste_item_view', ['token' => $token]);

    }

    /**
     * Fonction permettant de supprimer un item
     * @param $token token de l'item
     * @param $id id de l'item
     */
    public function supprimerItem($token, $id) {
        $item = Item::where('id','=',$id)->first();
        if (!$item)
            Alerte::createThenRedirect("Cet item n'existe pas", Alerte::WARNING, 'liste_user_detail', ['token' => $token]);
        if ($item->reserve==1)
            Alerte::createThenRedirect("Vous ne pouvez pas supprimer un item réservé", Alerte::WARNING, 'liste_user_detail', ['token' => $token]);
        if ($item->cagnotte()->first())
            $item->cagnotte()->first()->delete();
        $item->delete();
        Alerte::createThenRedirect("Item supprimé avec succès", Alerte::SUCCESS,'liste_user_detail', ['token' => $token]);
    }


}