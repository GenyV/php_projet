<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21/11/2018
 * Time: 18:49
 */

namespace mywishlist\Controleurs;


use mywishlist\Vues\Vue404;
use mywishlist\Vues\VueHome;

class ControleurPrincipal
{


    /**
     * Fonction renvoyant la page d'accueil
     */
    public function home() {
        echo (new VueHome())->renderAll();
    }

    /**
     * Affichage de la page 404 not found
     */
    public function fonctionNotFound() {
        echo (new Vue404())->renderAll();
    }

}