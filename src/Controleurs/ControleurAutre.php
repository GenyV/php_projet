<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/11/2018
 * Time: 21:44
 */

namespace mywishlist\Controleurs;

use mywishlist\Vues\VueCreateurs;

require_once ('vendor/autoload.php');
class ControleurAutre
{
    /**
     * Fonction permettant d'afficher la vue créateur
     */
    public function createurs() {
        echo (new VueCreateurs())->renderAll();
    }
}