<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22/11/2018
 * Time: 16:31
 */

namespace mywishlist\Controleurs;

use mywishlist\models\Liste;
use mywishlist\models\Profil;
use mywishlist\models\Users;
use mywishlist\Utils\Alerte;
use mywishlist\Utils\Utilisateur;
use mywishlist\Vues\VueChangerMdp;
use mywishlist\Vues\VueConnexion;
use mywishlist\Vues\VueInscription;
use mywishlist\Vues\VueListes;
use mywishlist\Vues\VueParticipation;
use mywishlist\Vues\VueProfil;
use mywishlist\Vues\VueSuppression;

require_once ('vendor/autoload.php');
class ControleurUtilisateur
{
    /**
     * Fonction permettant d'afficher le profil d'un utilisateur
     * @param $username username de l'utilisateur
     */
    public function afficherProfil($username) {
        if (!Users::where('username','=',$username)->first())
            Alerte::createThenRedirect("Cet utilisateur n'existe pas", Alerte::WARNING, 'root');
        echo (new VueProfil())->renderAll(['username' => $username]);
    }

    public function afficherListes($username) {
        echo (new VueListes())->renderAll(['username' => $username]);
    }


    public function modifierProfil($username) {
        if (!Utilisateur::estConnecte())
            Alerte::createThenRedirect("Vous devez être connecté", Alerte::WARNING, 'login');
        if (!Utilisateur::getUser()==$username)
            Alerte::createThenRedirect("Ce n'est pas votre profil !", Alerte::ERROR, 'root');
        if (isset($_POST['descProfil'])) {
            if (strlen($_POST['descProfil'])>255)
                Alerte::createThenRedirect("Votre description est trop longue", Alerte::WARNING, 'user_profil_view',['username' => $username]);
            $profil =  Utilisateur::getUser()->profil()->first();
            $profil->description = $_POST['descProfil'];
            $profil->save();
            Alerte::createThenRedirect("Description modifiée avec succès", Alerte::SUCCESS, 'user_profil_view',['username' => $username]);

        }
        if (count($_FILES)==1) {
            $extension = explode('.', $_FILES['fileImg']['name'])[count(explode($_FILES['fileImg']['name'], '.'))];
            if ($extension != "gif" && $extension != "png" && $extension != "jpg")
                Alerte::createThenRedirect("Extention non autorisée, extensions autorisées : .gif .png .jpg", Alerte::WARNING, 'user_profil_view',['username' => $username]);
            if ($_FILES['fileImg']['error'] > 0)
                Alerte::createThenRedirect("Erreur lors du transfert du fichier", Alerte::ERROR, 'user_profil_view',['username' => $username]);
            if ($_FILES['fileImg']['size'] > $_POST['MAX_FILE_SIZE'])
                Alerte::createThenRedirect("Erreur le fichier est trop volumineux", Alerte::ERROR, 'user_profil_view',['username' => $username]);
            $image_sizes = getimagesize($_FILES['fileImg']['tmp_name']);
            if ($image_sizes[0] != $image_sizes[1])
                Alerte::createThenRedirect("L'image doit être carré", Alerte::WARNING, 'user_profil_view',['username' => $username]);
            $fileName = 'profil.'.$extension;
            if (!file_exists('web/img/'.Utilisateur::getUser()->id)) {
                mkdir('web/img/'.Utilisateur::getUser()->id, 0777, true);
            }
            $transfert = move_uploaded_file($_FILES['fileImg']['tmp_name'], 'web/img/' . Utilisateur::getUser()->id . '/' . $fileName);
            $profil = Utilisateur::getUser()->profil()->first();
                if ($transfert) {
                        $profil->imgProfil = Utilisateur::getUser()->id . '/' . $fileName;
                        $profil->save();
                        Alerte::createThenRedirect("L'image a bien été changée", Alerte::SUCCESS, 'user_profil_view',['username' => $username]);
                } else {
                    Alerte::createThenRedirect("Erreur lors du transfert", Alerte::ERROR, 'user_profil_view',['username' => $username]);
                }
        } else if (isset($_POST['addliste'])) {
            $tokenModif = explode("/",$_POST['addliste'])[count(explode("/",$_POST['addliste']))-1];
            $liste = Liste::where('tokenModification','=',$tokenModif)->first();
            if (!$liste)
                Alerte::createThenRedirect("Cette liste n'existe pas", Alerte::WARNING, 'user_profil_view',['username' => $username]);

            if (!is_null($liste->user_id)) {
                if ($liste->user_id == Utilisateur::getUser()->id)
                    Alerte::createThenRedirect("Vous êtes déjà le propriétaire de cette liste", Alerte::WARNING, 'user_profil_view',['username' => $username]);
                Alerte::createThenRedirect("Cette liste a déjà un propriétaire", Alerte::WARNING, 'user_profil_view',['username' => $username]);
            }

            $liste->user_id=Utilisateur::getUser()->id;
            $liste->save();
            Alerte::createThenRedirect("Liste ajoutée avec succès", Alerte::SUCCESS, 'user_profil_view',['username' => $username]);
        }
    }

    public function supprimerImage($username) {
        if (!Utilisateur::estConnecte())
            Alerte::createThenRedirect("Vous devez être connecté", Alerte::WARNING, 'login');
        if (!Utilisateur::getUser()==$username)
            Alerte::createThenRedirect("Ce n'est pas votre profil !", Alerte::ERROR, 'root');
        $profil = Utilisateur::getUser()->profil()->first();
        $profil->imgProfil="";
        $profil->save();
        Alerte::createThenRedirect("L'image a bien été supprimée", Alerte::SUCCESS, 'user_profil_view',['username' => $username]);
    }

    public function supprimerDescription($username) {
        if (!Utilisateur::estConnecte())
            Alerte::createThenRedirect("Vous devez être connecté", Alerte::WARNING, 'login');
        if (!Utilisateur::getUser()==$username)
            Alerte::createThenRedirect("Ce n'est pas votre profil !", Alerte::ERROR, 'root');
        $profil = Utilisateur::getUser()->profil()->first();
        $profil->description="";
        $profil->save();
        Alerte::createThenRedirect("La description a bien été supprimée", Alerte::SUCCESS, 'user_profil_view',['username' => $username]);
    }

    public function  userParticipations($page) {
        if (!Utilisateur::estConnecte())
            Alerte::createThenRedirect("Vous devez être connecté pour accéder à votre historique de participation", Alerte::WARNING, 'root');
        $eltParPage=15;
        $pageMax = Utilisateur::getUser()->participations()->count()/$eltParPage+1;
        if ($page<$pageMax)
            $pageAct = $page;
        else
            $pageAct = 1;
        $pageAct=$page;
        if ($page==0)
            $pageAct=1;
        echo (new VueParticipation())->renderAll(['page' => $pageAct]);
    }


    public function inscription() {
        echo (new VueInscription())->renderAll();
    }

    public function traiterInscription() {
        if (Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous êtes déjà connecté', Alerte::INFO,'root');
        $username = $_POST['username'];
        $pwd = $_POST['pass'];
        if ($pwd != $_POST['pass2'])
            Alerte::createThenRedirect('Erreur les deux mots de passes sont differents', Alerte::WARNING, 'register');
        if (strlen($pwd)<8)
            Alerte::createThenRedirect('Erreur votre mot de passe est trop court 8 caractères minimum', Alerte::WARNING, 'register');
        if (!empty(Users::where('username','=',$username)->first()))
            Alerte::createThenRedirect('Nom d\'utilisateur déjà pris', Alerte::WARNING, 'register');
        $user = new Users();
        $user->username = $username;
        $hashed_password = hash('sha512', $pwd);
        $user->password = $hashed_password;
        $user->save();
        $profil = new Profil();
        $profil->id_user = $user->id;
        $profil->save();
        Alerte::createThenRedirect("Félicitation vous êtes maintenant inscrit, vous pouveau desormais vous connecter",Alerte::SUCCESS,'login');
        echo (new VueInscription())->renderAll();

    }

    public function connexion() {
        echo (new VueConnexion())->renderAll();
    }

    public function traiterConnexion() {
        if (Utilisateur::estConnecte()) {
            Alerte::createThenRedirect('Vous êtes déjà connecté', Alerte::INFO,'root');
        }
        $hashpwd =  hash('sha512', $_POST['pass']);
        $user = Users::where('username','=',$_POST['username'])->first();
        if (!$user)
            Alerte::createThenRedirect('Nom d\'utilisateur inexistant', Alerte::WARNING, 'login');
        if ($hashpwd != $user->password) {
            Alerte::createThenRedirect('Mot de passe incorrect', Alerte::WARNING, 'login');
        }

        Utilisateur::saveUser($user);
        setcookie("user_id", $user->id);
        Alerte::createThenRedirect("Bienvenue ".$user->username,Alerte::SUCCESS,'root');

    }

    public function afficherChangementMdp($username){
        if(!Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous devez être connecté pour changer de mot de passe', Alerte::WARNING,'root');
        if ($username =! Utilisateur::getUser()->username)
            Alerte::createThenRedirect("Vous n'êtes pas le propriétaire de ce compte !", Alerte::ERROR, 'root');
        echo (new VueChangerMdp())->renderAll();

    }

    public function changerMdp() {
        if(!Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous devez être connecté pour changer de mot de passe', Alerte::WARNING,'root');
        if ($username =! Utilisateur::getUser()->username)
            Alerte::createThenRedirect("Vous n'êtes pas le propriétaire de ce compte !", Alerte::ERROR, 'root');
        $user = Utilisateur::getUser();
        if (hash('sha512', $_POST['oldpass'])!= $user ->password)
            Alerte::createThenRedirect('Mot de passe incorrect', Alerte::WARNING,'user_mdp_change');
        $pwd = $_POST['pass'];
        if ($pwd != $_POST['pass2'])
            Alerte::createThenRedirect('Erreur les nouveaux mots de passe ne correspondent pas', Alerte::WARNING, 'user_mdp_change');
        if (strlen($pwd)<8)
            Alerte::createThenRedirect('Erreur votre mot de passe est trop court, 8 caractères minimum', Alerte::WARNING, 'user_mdp_change');
        $hashed_password = hash('sha512', $pwd);
        $user->password = $hashed_password;
        $user->save();
        Utilisateur::deconnecter();
        Alerte::createThenRedirect("Mot de passe modifié avec succès",Alerte::SUCCESS,'login');
    }

    public function afficherSuppression($username){
        if ($username != Utilisateur::getUser()->username)
            Alerte::createThenRedirect("Vous n'êtes pas le propriétaire de ce compte !", Alerte::ERROR, 'root');
        echo (new VueSuppression())->renderAll();
    }

    public function supprimerCompte($username){
        if (!Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous devez être connecté pour supprimer votre compte', Alerte::WARNING,'root');
        if ($username != Utilisateur::getUser()->username)
            Alerte::createThenRedirect("Vous n'êtes pas le propriétaire de ce compte !", Alerte::ERROR, 'root');
        $user = Utilisateur::getUser();
        foreach($user->participations()->get() as $part){
            $liste = $part->item()->first()->liste()->first();
            $part->message = "";
            $part->save();
            if ($liste->expiration>date('Y-m-d')) {
                $item = $part->item()->first();
                $item->reserve = 0;
                $item->save();
                $part->delete();
            }
        }
        foreach ($user->listes()->get() as $liste){
            foreach ($liste->items()->get() as $item) {
                $item->delete();
            }
            $liste->delete();
        }
		$user->profil()->delete();
        Utilisateur::deconnecter();
        $user->delete();
        Alerte::createThenRedirect("Compte supprimé avec succès",Alerte::SUCCESS,'root');
    }

    public function deconnexion() {
        Utilisateur::deconnecter();
        Alerte::createThenRedirect("Vous avez été déconnecté",Alerte::INFO,'root');
    }


}