<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21/11/2018
 * Time: 19:17
 */

namespace mywishlist\Controleurs;


use mywishlist\models\Liste;
use mywishlist\models\Users;
use mywishlist\Utils\Alerte;
use mywishlist\Utils\Utilisateur;
use mywishlist\Vues\VueCreateList;
use mywishlist\Vues\VueListes;
use mywishlist\Vues\VueRechercher;
use mywishlist\Vues\VueUserListes;

/**
 * Class ControleurListe
 * @package mywishlist\Controleurs
 */
class ControleurListe
{
    /**
     * Fonction permettant d'afficher la vue des listes normale
     */
    public function liste() {
        echo (new VueListes())->renderAll();
    }


    /**
     * Fonction permettant de valider la liste
     * @param $token token de modification de la liste
     */
    public function validerListe($token) {
        $liste = Liste::where('tokenModification','=',$token)->first();
        if (!$liste)
            Alerte::createThenRedirect("Liste inexistante",Alerte::WARNING,"Liste inexistante",Alerte::WARNING,'user_liste_view');
        $liste->valide=1;
        $liste->save();
        Alerte::createThenRedirect("Liste validée avec succès",Alerte::SUCCESS,'user_liste_view');
    }

    /**
     * Fonction permettant d'afficher la page de modification de la liste
     * @param $token token de modification de la liste
     */
    public function afficherModifierListe($token) {
        echo (new VueCreateList())->renderAll(['token' => $token]);
    }

    /**
     * Fonction permettant de modifier une liste (est appelée par un post)
     * @param $token
     */
    public function modifierListe($token) {
        $liste = Liste::where('tokenModification','=',$token)->first();
        if (!$liste)
            Alerte::createThenRedirect("Liste inexistante",Alerte::WARNING,"Liste inexistante",Alerte::WARNING,'user_liste_view');
        if ($this->correctInput($_POST["nomListe"]) && $this->correctInput( $_POST["descListe"]) && $this->correctInput($_POST["dateListe"])) {
            $liste->titre = $_POST["nomListe"];
            $liste->description = $_POST["descListe"];
            $liste->expiration = $_POST["dateListe"];
            if (isset($_POST["publique"]))
                $liste->publique=1;
            else
                $liste->publique=0;
            $liste->message = $_POST["messageListe"];
            $liste->save();
            Alerte::createThenRedirect("Liste modifiée avec succès",Alerte::SUCCESS,'user_liste_view');
        } Alerte::createThenRedirect("Caracteres '<' ou '>' interdits",Alerte::WARNING,'liste_modify_view',array('token' => $liste->tokenModification));
    }

    /**
     * Fonction permettant d'afficher les items dans une liste
     * @param $token token de la liste
     */
    public function itemsInListe($token) {
        echo (new VueListes())->renderAll(['token' => $token]);
    }

    /**
     * Fonction permettant d'afficher les items dans une liste personnelle
     * @param $token token de la liste
     */
    public function itemsInUserListe($token) {
        echo (new VueUserListes())->renderAll(['token' => $token]);
    }

    /**
     * Fonction permettant de partager une liste
     * @param $token token de la liste
     */
    public function share($token) {
        $liste = Liste::where('tokenModification','=',$token)->first();
        $sharelink =$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].BASE_URL.'/listes/'.$liste->token;
        echo (new VueUserListes())->renderAll(array('token' => $token, 'sharelink' => $sharelink));

    }

    /**
     * Fonction permettant de supprimer une liste
     * @param $token token de la liste
     */
    public function delete($token) {
        $liste = Liste::where('token','=',$token)->first();
        if (!$liste)
            Alerte::createThenRedirect("Liste inexistante",Alerte::WARNING,'user_liste_view');
        /*foreach ($liste->items()->get() as $item) {
            $item->delete();
        }*/

        $liste->delete();
        Alerte::createThenRedirect("Liste supprimée avec succès", Alerte::SUCCESS,'user_liste_view');
    }

    /**
     * Fonction permettant d'afficher les listes de l'utilisateur
     */
    public function userListes() {
        if (!Utilisateur::estConnecte())
            Alerte::createThenRedirect("Vous devez être connecté pour accéder à vos listes", Alerte::WARNING, 'root');
        echo (new VueUserListes())->renderAll();
    }

    /**
     * Fonction permettant de changer le niveau d'acces de la liste
     * @param $token token de la liste
     * @param $acces niveau d'acces
     */
    public function chgtAcces($token, $acces){
        $liste = Liste::where('tokenModification','=',$token);
        if (!$liste)
            Alerte::createThenRedirect("Liste inexistante", Alerte::WARNING,'user_liste_view');
        if ($acces!=1 && $acces!=0)
            Alerte::createThenRedirect("Niveau d'accès incorrect",Alerte::WARNING,'user_liste_view');
        $liste->update(['publique' => $acces]);
        Alerte::createThenRedirect("Niveau d'accessibilité mis à jour avec succès", Alerte::SUCCESS,'liste_user_detail',['token' => $token]);
    }

    public function vueCreerListe(){
        echo (new VueCreateList())->renderAll();
    }

    /**
     * Fonction permettant d'afficher la vue pour creer une liste
     */
    public function creerListe(){
        if ($this->correctInput($_POST["nomListe"]) && $this->correctInput( $_POST["descListe"]) && $this->correctInput($_POST["dateListe"])) {
            $liste = new Liste();
            $liste->titre = $_POST["nomListe"];
            $liste->description = $_POST["descListe"];
            $liste->expiration = $_POST["dateListe"];
            $liste->message = $_POST["messageListe"];
            if (isset($_POST["publique"]))
                $liste->publique = 1;
            if (Utilisateur::estConnecte())
                $liste->user_id = Utilisateur::getUser()->id;
            $liste->token = bin2hex(random_bytes(25));;
            $liste->tokenModification = bin2hex(random_bytes(50));
            $liste->save();
            if (!Utilisateur::estConnecte()) {
                if (isset($_COOKIE['listes'])) {
                    $array = unserialize($_COOKIE['listes']);
                    $array[$liste->no] = $liste->tokenModification;
                    setcookie('listes', serialize($array),0,"/");
                } else {
                    $array = [$liste->no => $liste->tokenModification];
                    setcookie('listes', serialize($array),0,"/");
                }
            }

            $editlink = $_SERVER['HTTP_ORIGIN']. BASE_URL . '/listes/edit/' . $liste->tokenModification;

            Alerte::createThenRedirect('Liste créée avec succès, lien de modification : <a href="' . $editlink.'">'.$editlink.'</a>', Alerte::SUCCESS, 'listes');
        }Alerte::createThenRedirect("Caracteres '<' ou '>' interdits",Alerte::WARNING,'listes_creer');
    }

    /**
     * Fonction permettant d'afficher la vue de recherche
     */
    public function vueChercherListe(){
        echo (new VueRechercher())->renderAll();
    }

    /**
     * Fonction permettant d'effectuer une recherche (appelée après le post de la vue VueRechercher
     */
    public function rechercher(){
        $auteur_id=-1;
        if ($_POST['nomAuteur']=="" && $_POST['date1']=="" && $_POST['date2']=="")
            Alerte::createThenRedirect("Vous devez replir l'un des champs", Alerte::WARNING, 'listes_search');
        if ($_POST['date1']!="" && $_POST['date2']=="")
            Alerte::createThenRedirect("Vous devez entrer une date de fin de recherche", Alerte::WARNING, 'listes_search');
        if ($_POST['date1']=="" && $_POST['date2']!="")
            Alerte::createThenRedirect("Vous devez entrer une date de début de recherche", Alerte::WARNING, 'listes_search');
        if ($_POST['nomAuteur']!="") {
            $auteur = Users::where('username','=',$_POST['nomAuteur'])->first();
            if (!$auteur)
                Alerte::createThenRedirect("Utilisateur inexistant", Alerte::WARNING, 'listes_search');
            $auteur_id=$auteur->id;
        }
        if ($_POST['date1']!="" && $_POST['date2']!="") {
            if ($_POST['date1']>$_POST['date2'])
                Alerte::createThenRedirect("La date de début doit être avant la date de fin", Alerte::WARNING, 'listes_search');
        }
        echo (new VueListes())->renderAll(array('auteur_id' => $auteur_id, 'date1' => $_POST['date1'], 'date2' => $_POST['date2']));
    }

    /**
     * Fonction permettant de savoir si l'input ne contient pas de balise html
     * @param $input texte a verifier
     * @return bool true si l'input est correct
     */
    public function correctInput($input) {
        if (strpos($input, '<')!==false || strpos($input, '>')!==false){
            return false;
        } return true;
    }
}