<?php
/**
 * Created by PhpStorm.
 * User: geny17u
 * Date: 20/11/2018
 * Time: 11:17
 */
namespace mywishlist\models;
require_once ('vendor/autoload.php');

class Item extends
    \Illuminate\Database\Eloquent\Model
{
    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * fonction permettant de renvoyer la liste de l'item
     */
    public function liste() {
       return $this->belongsTo( 'mywishlist\models\Liste','liste_id');
    }

    public function participation() {
        return $this->hasOne('mywishlist\models\Participation', 'id_item');
    }

    public function cagnotte() {
        return $this->hasOne('mywishlist\models\Cagnotte', 'id_item');
    }
}