<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22/11/2018
 * Time: 17:39
 */

namespace mywishlist\models;

require_once ("vendor/autoload.php");
class Users extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'users';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function listes() {
        return $this->hasMany('mywishlist\models\Liste', 'user_id') ;
    }

    public function participations() {
        return $this->hasMany('mywishlist\models\Participation', 'id_user') ;
    }

    public function profil() {
        return $this->hasOne('mywishlist\models\Profil', 'id_user') ;
    }


}