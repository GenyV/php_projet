<?php
/**
 * Created by PhpStorm.
 * User: geny17u
 * Date: 20/11/2018
 * Time: 11:17
 */
namespace mywishlist\models;

require_once ("vendor/autoload.php");
class Liste extends
    \Illuminate\Database\Eloquent\Model
{
    const PRIVE=0;
    const PUBLIC=1;

    protected $table = 'liste';
    protected $primaryKey = 'no';
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function items() {
        return $this->hasMany('mywishlist\models\Item', 'liste_id') ;
    }

    public function proprietaire() {
        return $this->belongsTo('mywishlist\models\Users', 'user_id');
    }

    public static function getItemsFromList($id) {
        $liste = Liste::where('no','=',$id)->first();
        echo "<center> Liste : ".$liste['titre']." </center>";
        foreach ($liste->items()->get() as $item) {
            echo "<b>Nom : </b>".$item['nom']."<br>";
            echo "<b>Description : </b>".$item['descr']."<br>";
            echo "<b>Tarif : </b>".$item['tarif']."<br>";
            echo "<br><br>";
        }
    }
}