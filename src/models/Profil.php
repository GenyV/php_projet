<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 25/11/2018
 * Time: 21:43
 */

namespace mywishlist\models;

require_once ('vendor/autoload.php');
class Profil extends
    \Illuminate\Database\Eloquent\Model
{
    protected $table = 'profil';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function user() {
        return $this->belongsTo( '\mywishlist\models\Users','id_user');
    }
}