<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23/11/2018
 * Time: 23:29
 */

namespace mywishlist\models;

class Participation extends
    \Illuminate\Database\Eloquent\Model
{
    protected $table = 'participation';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function user() {
        return $this->belongsTo( '\mywishlist\models\Users','id_user');
    }

    public function item() {
        return $this->belongsTo( '\mywishlist\models\Item','id_item');
    }
}