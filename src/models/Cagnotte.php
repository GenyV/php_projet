<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 24/11/2018
 * Time: 12:34
 */

namespace mywishlist\models;


class Cagnotte extends
    \Illuminate\Database\Eloquent\Model
{
    protected $table = 'cagnotte';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function item() {
        return $this->belongsTo( '\mywishlist\models\Item','id_item');
    }
}