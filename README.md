# README #

Ce README a pour but d'expliquer l'installation et le lancement du site

### Installer la base de données ###

* La base de données utilise le SGBD MySQL
* Importez le fichier mywishlist.sql

### Lancer le site ###

* Dans le fichier src/conf/conf.ini entrez vos informations de connexion à votre base de données

#### En local ####
* Importez et lancez la base de données
* Éxécutez php -S localhost:8080 -t . dans la racine du projet
* Tapez localhost:8080 dans la barre d'url de votre navigateur

#### Sur un serveur ####
* Importez et lancez la base de données
* Importez le projet dans le dossier www
* Lancez votre serveur
* Connectez vous y

* Le site est actuellement hebergé sur  : http://piccadillygame.ddns.net:8080/Wishlist/ 
* Pour tout problème de connexion : vanessa.geny@hotmail.fr

### Fonctionnalités ###

* Le fichier fonctionnalités.odt recense toutes les fonctionnalités du site

### Auteur ###

* [Vanessa GENY](https://www.linkedin.com/in/vanessa-geny-050bb113a/) (vanessa.geny8@etu.univ-lorraine.fr)