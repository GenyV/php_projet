<?php
include 'src/conf/config.php';
/**
 * Created by PhpStorm.
 * User: geny17u
 * Date: 20/11/2018
 * Time: 11:17
 */
require_once 'vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use mywishlist\Controleurs as c;
session_start();
$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

/**
 * Application Slim
 */
$app = new Slim\Slim();


/**
 * Routes
 */

$app->notFound(function () {
    (new c\ControleurPrincipal())->fonctionNotFound();
}) ;

/**
 * Route vers le home
 */
$app->get('/',function(){
    (new c\ControleurPrincipal())->home();
})->name('root') ;


/**
 * Routes vers les listes
 */



$app->get('/listes/',function(){
    (new c\ControleurListe())->liste();
})->name('listes');


$app->get('/listes/:token', function($token){
    (new c\ControleurListe())->itemsInListe($token);
})->name('liste_view');

$app->get('/listes/creer/', function(){
    (new c\ControleurListe())->vueCreerListe();
})->name('listes_creer');

$app->post('/listes/creer/', function(){
    (new c\ControleurListe())->creerListe();
})->name('listes_creation');

$app->get('/listes/search/', function(){
    (new c\ControleurListe())->vueChercherListe();
})->name('listes_search_view');

$app->post('/listes/search/', function(){
    (new c\ControleurListe())->rechercher();
})->name('listes_search');




/**
 * Routes de gestion du profil
 */

$app->get('/profil/:username', function($username){
    (new c\ControleurUtilisateur())->afficherProfil($username);
})->name('user_profil_view');

$app->get('/profil/:username/listes', function($username){
    (new c\ControleurUtilisateur())->afficherListes($username);
})->name('user_profil_listes_view');

$app->post('/profil/:username', function($username){
    (new c\ControleurUtilisateur())->modifierProfil($username);
})->name('user_profil_modifyProfil');

$app->get('/profil/:username/deleteImgProfil', function($username){
    (new c\ControleurUtilisateur())->supprimerImage($username);
})->name('user_profil_deleteImg');

$app->get('/profil/:username/deleteDescProfil', function($username){
    (new c\ControleurUtilisateur())->supprimerDescription($username);
})->name('user_profil_deleteDesc');

$app->get('/profil/:username/edit', function($username){
    (new c\ControleurUtilisateur())->afficherChangementMdp($username);
})->name('user_mdp_change');

$app->post('/profil/:username/edit', function($username){
    (new c\ControleurUtilisateur())->changerMdp();
})->name('user_mdp_check');

$app->get('/profil/participations/:page', function($page){
    (new c\ControleurUtilisateur())->userParticipations($page);
})->name('user_participation_view');

$app->get('/profil/listes/', function(){
    (new c\ControleurListe())->userListes();
})->name('user_liste_view');

$app->get('/listes/edit/:token', function($token){
    (new c\ControleurListe())->itemsInUserListe($token);
})->name('liste_user_detail');

$app->get('/listes/edit/:token/validate', function($token){
    (new c\ControleurListe())->validerListe($token);
})->name('liste_validate');

$app->get('/listes/edit/:token/modify', function($token){
    (new c\ControleurListe())->afficherModifierListe($token);
})->name('liste_modify_view');

$app->post('/listes/edit/:token/modify', function($token){
    (new c\ControleurListe())->modifierListe($token);
})->name('liste_modify');

$app->get('/listes/edit/:token/share', function($token){
    (new c\ControleurListe())->share($token);
})->name('liste_share');

$app->get('/listes/edit/:token/delete', function($token){
    (new c\ControleurListe())->delete($token);
})->name('liste_suppr');

$app->get('/listes/edit/:token/acces/:acces', function($token, $acces){
    (new c\ControleurListe())->chgtAcces($token, $acces);
})->name('listes_acces');


/**
 * Routes de gestion des items
 */

$app->get('/listes/edit/:token/addItem', function($token){
    (new c\ControleurItem())->afficherAjoutItem($token);
})->name('liste_item_addView');

$app->post('/listes/edit/:token/addItem', function($token){
    (new c\ControleurItem())->ajouterItem($token);
})->name('liste_item_add');

$app->get('/listes/item/:token', function($token){
    (new c\ControleurItem())->afficherItem($token);
})->name('liste_item_view');

$app->post('/listes/item/:token', function($token){
    (new c\ControleurItem())->modifierImageOuParticiper($token);
})->name('liste_item_other');

$app->get('/listes/item/:token/deleteImage', function($token){
    (new c\ControleurItem())->supprimerImage($token);
})->name('liste_item_image_delete');

$app->get('/listes/edit/:token/modifyItem/:id', function($token, $id){
    (new c\ControleurItem())->afficherModifierItem($token ,$id);
})->name('liste_item_modify_view');

$app->post('/listes/edit/:token/modifyItem/:id', function($token, $id){
    (new c\ControleurItem())->modifierItem($token ,$id);
})->name('liste_item_modify');

$app->get('/listes/item/:token/reserver', function($token){
    (new c\ControleurItem())->afficherReserverItem($token);
})->name('liste_item_reserve_view');

$app->get('/listes/item/:token/cagnotte/creer', function($token){
    (new c\ControleurItem())->creerCagnotte($token);
})->name('liste_item_cagnotte_create');

$app->post('/listes/item/:token/reserver', function($token){
    (new c\ControleurItem())->reserverItem($token);
})->name('liste_item_reserve');

$app->get('/listes/edit/:token/deleteItem/:id', function($token, $id){
    (new c\ControleurItem())->supprimerItem($token ,$id);
})->name('liste_item_delete');

/**
 * Routes de gestion de l'utilisateur
 */

$app->get('/register/', function() {
    (new c\ControleurUtilisateur())->inscription();
})->name('register');

$app->post('/register/', function() {
    (new c\ControleurUtilisateur())->traiterInscription();
})->name('checkRegister');

$app->get('/login', function() {
    (new c\ControleurUtilisateur())->connexion();
})->name('login');

$app->post('/login', function() {
    (new c\ControleurUtilisateur())->traiterConnexion();
})->name('checkLogin');

$app->get('/logout', function() {
    (new c\ControleurUtilisateur())->deconnexion();
})->name('logout');

$app->get('/profil/:username/delete', function($username){
    (new c\ControleurUtilisateur())->afficherSuppression($username);
})->name('user_delete');

$app->get('/profil/:username/delete/deleted', function($username){
    (new c\ControleurUtilisateur())->supprimerCompte($username);
})->name('user_delete_check');

/**
 * Routes autres
 */

$app->get('/createurs/', function() {
    (new c\ControleurAutre())->createurs();
})->name('createurs_view');

/**
 * Lancement de l'application Slim
 */
$app->run() ;

?>
